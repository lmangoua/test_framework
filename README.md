# test_framework

Sample framework to test Web UI, using Java, Gradle, TestNG and Allure Report for reporting.
This is a #Skeleton Framework (used for DigitOutsource assessment)

*Generate html Allure Reports:
- $ allure serve ./build/allure-results/
Run it in the terminal after running the code. Do it from the project root. (e.g. $ cd  test_framework, then $ allure serve ./build/allure-results/)