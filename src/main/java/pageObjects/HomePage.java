package main.java.pageObjects;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

import main.java.testClasses.Home;
import main.java.utils.DriverFactory;
import main.java.utils.SeleniumUtility;
import org.openqa.selenium.By;

public class HomePage extends DriverFactory {

    static By hotelBookingFormLabel = By.xpath(property.returnPropVal(fileName, "hotelBookingFormLabelXpath"));
    static By firstnameLabel = By.xpath(property.returnPropVal(fileName, "firstnameLabelXpath"));
    static By surnameLabel = By.xpath(property.returnPropVal(fileName, "surnameLabelXpath"));
    static By firstNameTextbox = By.xpath(property.returnPropVal(fileName, "firstNameTextboxXpath"));
    static By lastNameTextbox = By.xpath(property.returnPropVal(fileName, "lastNameTextboxXpath"));
    static By totalPriceTextbox = By.xpath(property.returnPropVal(fileName, "totalPriceTextboxXpath"));
    static String depositPaidDropdownList = property.returnPropVal(fileName, "depositPaidDropdownListXpath");
    static By checkInTextbox = By.xpath(property.returnPropVal(fileName, "checkInTextboxXpath"));
    static By checkOutTextbox = By.xpath(property.returnPropVal(fileName, "checkOutTextboxXpath"));
    static By saveButton = By.xpath(property.returnPropVal(fileName, "saveButtonXpath"));
    static String firstname = "John";
    static String surname = "Wick";
    static int price = 1000;
    static String totalPrice;
    static String checkin = "2019-07-21";
    static String checkout = "2019-08-21";
    static String option = "true";
    static String bookingTable = property.returnPropVal(fileName, "bookingTableXpath");
    static String validateContactIsCreated = property.returnPropVal(fileName, "validateContactIsCreatedXpath");
    static String deleteContactButton = property.returnPropVal(fileName, "deleteContactButtonXpath");

    public static void goToUrl() {

        navigateTo(url);

        validateHomePage();
    }

    public static void fillInForm() {

        enterDetails();
    }

    public static void validateBookingCreated() {

        validateBooking();
    }

    public static void deleteBookingCreated() {

        deleteBooking();
    }

    //region <Navigate to URL>
    public static void navigateTo(String url) {

        driver.navigate().to(url);

        System.out.println("\nNavigated to " + url + " successfully \n");
    }
    //endregion

    //region <Validate Home Page>
    public static void validateHomePage() {

        //wait for Hotel booking form label
        SeleniumUtility.waitForElement(hotelBookingFormLabel, "Failed to wait for Hotel booking form label");

        //wait for Firstname label
        SeleniumUtility.waitForElement(firstnameLabel, "Failed to wait for Firstname label");

        //wait for Surname label
        SeleniumUtility.waitForElement(surnameLabel, "Failed to wait for Surname label");

        System.out.println("\nValidated Home Page successfully \n");
    }
    //endregion

    //region <Enter Details>
    public static void enterDetails() {

        //enter Firstname
        SeleniumUtility.enterText(firstNameTextbox, firstname, "Failed to enter Firstname");

        //enter Surname
        SeleniumUtility.enterText(lastNameTextbox, surname, "Failed to enter Surname");

        //enter Price
        totalPrice = String.valueOf(price);
        SeleniumUtility.enterText(totalPriceTextbox, totalPrice, "Failed to enter Price");

        //select Deposit option
//        SeleniumUtility.selectFromDropdownListViaDivUsingXpath(depositPaidDropdownList, option, "Failed to select Deposit option \"" + option + "\"");
//        SeleniumUtility.enterText(passwordExpressTextBox, password, "Failed to select Deposit option");

        //enter check-in
        SeleniumUtility.enterText(checkInTextbox, checkin, "Failed to enter check-in");

        //enter check-out
        SeleniumUtility.enterText(checkOutTextbox, checkout, "Failed to enter check-out");

        //click Save button
        SeleniumUtility.clickElement(saveButton, "Failed to click Save button");

        System.out.println("\nCreated a booking successfully \n");
    }
    //endregion

    //region <Validate Booking>
    public static void validateBooking() {

        //Validate
//        SeleniumUtility.waitForOptionByXpath(validateContactIsCreated, "Failed to validate Booking");
        SeleniumUtility.waitForElementByXpath(Home.validateContactCreated(firstname, surname, totalPrice, option, checkin, checkout), "Failed to validate Booking");

        System.out.println("\nValidated Booking successfully \n");
    }
    //endregion

    //region <Delete Booking>
    public static void deleteBooking() {

        //click Delete
//        SeleniumUtility.doubleClickElementByXpath(deleteContactButton, "Failed to delete Booking");
        SeleniumUtility.doubleClickElementByXpath(Home.deleteContact(firstname, surname, totalPrice, option, checkin, checkout), "Failed to click delete button");


        System.out.println("\nDeleted Booking successfully \n");
    }
    //endregion
}
