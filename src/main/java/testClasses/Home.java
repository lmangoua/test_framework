package main.java.testClasses;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

/**
 * Option 2 to store element's attributes
 */
public class Home {

    public static String validateContactCreated(String firstname, String surname, String price, String option, String checkin, String checkout) {
        return "//div/p[contains(text(), '" + firstname + "')]/../..//p[contains(text(), '" + surname + "')]/../..//p[contains(text(), '" + price + "')]/../..//p[contains(text(), '" + option + "')]/../..//p[contains(text(), '" + checkin + "')]/../..//p[contains(text(), '" + checkout + "')]";
    }

    public static String deleteContact(String firstname, String surname, String price, String option, String checkin, String checkout) {
        return "//div/p[contains(text(), '" + firstname + "')]/../..//p[contains(text(), '" + surname + "')]/../..//p[contains(text(), '" + price + "')]/../..//p[contains(text(), '" + option + "')]/../..//p[contains(text(), '" + checkin + "')]/../..//p[contains(text(), '" + checkout + "')]/../..//input[@value='Delete']";
    }
}



