package main.java.utils;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    public static String Execution_Web = System.getProperty("EXECUTION_WEB");
    public static RemoteWebDriver driver; //For web
    public static WebDriverWait wait = null;
    public static int waitTime = 60;
    public static String BROWSER = System.getProperty("BROWSER");
    public static String PLATFORM = System.getProperty("PLATFORM");
    public DesiredCapabilities capabilities;
    public ChromeOptions options;
    public String testName;
    public static final Logger LOGGER = LoggerFactory.getLogger(DriverFactory.class);
    public static PropertyFileReader property = new PropertyFileReader();
    public static String fileName = "Home";
    public static String url = property.returnPropVal(fileName, "homePageUrl");

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeSuite()
    //region <setupWebDriver>
    public void setupWebDriver() throws MalformedURLException {

        switch(Execution_Web.toLowerCase()){

            case("remote"):
//                setupRemoteDriver();
                break;
            case("local"):
            default:
                setupLocalDriver();
                break;
        }

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, waitTime);
    }
    //endregion

    //region <setupLocalDriver>
    private void setupLocalDriver() throws MalformedURLException {

        switch (BROWSER) {

            case ("Firefox"):
                LOGGER.info("\nWeb test is Starting ... \n");

                String geckoPath = "src/main/resources/geckodriver";
                String geckoAbsolutePath;
                File geckoFile = new File(geckoPath);
                System.out.println("Gecko driver directory - " + geckoFile.getAbsolutePath());
                geckoAbsolutePath = geckoFile.getAbsolutePath();

                System.setProperty("webdriver.gecko.driver", geckoAbsolutePath + "");

                FirefoxOptions firefoxOptions = new FirefoxOptions();

                driver = new FirefoxDriver(firefoxOptions);
                LOGGER.info(BROWSER + " on local machine initiated \n");
                break;
            case ("internetexplorer"):
                LOGGER.info("\nWeb test is Starting ... \n");

                capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability("name", testName);
                capabilities.setCapability("command-timeout", "300");
                capabilities.setCapability("idle-timeout", "120");
                capabilities.setBrowserName("iexplore");
                capabilities.setCapability("platform", PLATFORM);

                driver = new InternetExplorerDriver(capabilities);
                LOGGER.info(BROWSER + " on local machine initiated \n");
                break;
            case ("Chrome"):
                LOGGER.info("\nWeb test is Starting ... \n");

                String chromePath = "src/main/resources/chromedriver";
                String chromeAbsolutePath;
                File chromeFile = new File(chromePath);
                System.out.println("Chrome driver directory - " + chromeFile.getAbsolutePath());
                chromeAbsolutePath = chromeFile.getAbsolutePath();

                System.setProperty("webdriver.chrome.driver", chromeAbsolutePath + "");

                options = new ChromeOptions();

                options.addArguments("--disable-extensions");
                options.addArguments("disable-infobars");
                options.addArguments("test-type");
                options.addArguments("enable-strict-powerful-feature-restrictions");
                options.setCapability(ChromeOptions.CAPABILITY, options);
                options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

                driver = new ChromeDriver(options);
                LOGGER.info(BROWSER + " on local machine initiated \n");
                break;
        }
    }
    //endregion

    @AfterSuite(alwaysRun = true)
    //region <tearDown>
    public void tearDown() throws IOException {

        switch (Execution_Web.toLowerCase()) {

            case ("remote"):
                boolean hasQuit = driver.toString().contains("(null)");
                if (driver != null || hasQuit == false) {
                    LOGGER.info("Test is Ending ...\n");

                    driver.quit();
                }

                break;
            default:
                if (driver != null) {
                    LOGGER.info("Test is Ending ...\n");

                    driver.quit();
                }

                break;
        }
    }
    //endregion
}
