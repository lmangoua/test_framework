package main.java.utils;

/**
 * @author: lmangoua
 * date: 05/06/19
 */

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class SeleniumUtility extends DriverFactory {

    public static int WaitTimeout = 6;

    //region <alertHandler>
    public static void alertHandler() {

        try {
            System.out.println("Attempting to click OK in alert pop-up");

            //Get a handle to the open alert, prompt or confirmation
            Alert alert = driver.switchTo().alert();

            //Get the text of the alert or prompt
            alert.getText();

            //And acknowledge the alert (equivalent to clicking "OK")
            alert.accept();

            System.out.println("Ok Clicked successfully...proceeding");
            Assert.assertTrue("Ok Clicked successfully...proceeding", true);
        }
        catch (Exception e) {
            System.out.println("Failed to click OK in alert pop-up - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to click OK in alert pop-up - " + e.getMessage());
        }
    }
    //endregion

    //region <clearTextAndEnterValueByXpath>
    public static void clearTextAndEnterValueByXpath(String elementXpath, String textToEnter, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "a")); //Ctrl+a
            elementToTypeIn.sendKeys(Keys.DELETE); //Delete
            Actions typeText = new Actions(driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            elementToTypeIn = driver.findElement(By.xpath(elementXpath));

            elementToTypeIn.click();

            String retrievedText = elementToTypeIn.getAttribute("value");

            if (retrievedText != null) {

                if (retrievedText.equals(textToEnter)) {
                    System.out.println("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
                else {
                    System.out.println("Text entered does not match text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
            }
            else {
                System.out.println("Null value Found");
                Assert.fail("\n[ERROR] Null value Found ---  ");
            }
        }
        catch (Exception e) {
            System.out.println("Error entering text - " + elementXpath + " - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clearTextAndEnterValue>
    public static void clearTextAndEnterValue(By element, String textToEnter, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = driver.findElement(element);
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "a")); //Ctrl+a
            elementToTypeIn.sendKeys(Keys.DELETE); //Delete
            Actions typeText = new Actions(driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            elementToTypeIn = driver.findElement(element);

            elementToTypeIn.click();

            String retrievedText = elementToTypeIn.getAttribute("value");

            if (retrievedText != null) {

                if (retrievedText.equals(textToEnter)) {
                    System.out.println("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
                else {
                    System.out.println("Text entered does not match text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
            }
            else {
                System.out.println("Null value Found");
                Assert.fail("\n[ERROR] Null value Found ---  ");
            }
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clearTextByXpath>
    public static void clearTextByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();

            System.out.println("Cleared text on element by Xpath : " + elementXpath);
            Assert.assertTrue("Cleared text on element by Xpath : " + elementXpath, true);
        }
        catch (Exception e) {
            System.out.println("Error clearing text - " + elementXpath + " - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to clear text on element by Xpath  ---  " + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <clearText>
    public static void clearText(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = driver.findElement(element);
            elementToTypeIn.clear();

            System.out.println("Cleared text on element : " + element);
            Assert.assertTrue("Cleared text on element : " + element, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to clear text on element --- " + element + " - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElementByXpath>
    public static void clickElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            System.out.println("Clicked element by Xpath : " + elementXpath);
            Assert.assertTrue("Clicked element by Xpath : " + elementXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to click on element by Xpath - " + elementXpath + "' - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElement>
    public static void clickElement(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            WebElement elementToClick = driver.findElement(element);
            elementToClick.click();

            System.out.println("Clicked element : " + element);
            Assert.assertTrue("Clicked element : " + element, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to click on element --- " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickOptionByXpath>
    /*Dynamic method to click an element by Xpath. Using a dynamic Xpath of the element, we just specify the value to click*/
    public static void clickOptionByXpath(String element, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(element, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(updatedXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(updatedXpath)));
            WebElement elementToClick = driver.findElement(By.xpath(updatedXpath));
            elementToClick.click();

            System.out.println("Clicked element by Xpath : " + updatedXpath);
            Assert.assertTrue("Clicked element by Xpath : " + updatedXpath, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to click on element by Xpath --- " + updatedXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <copyKeys>
    public static void copyKeys() {

        try {
            Actions action = new Actions(driver);
            action.sendKeys(Keys.CONTROL, "c"); //Ctrl+c
            action.perform();
        }
        catch (Exception e) {
            System.out.println("Failed to send keypress to element - Control + C");
            Assert.fail("\n[ERROR] Failed to send keypress to element - Control + C --- " + e.getMessage());
        }
    }
    //endregion

    //region <doubleClickElementByXpath>
    public static void doubleClickElementByXpath(String elementLinkTextXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementLinkTextXpath, errorMessage);
            Actions act = new Actions(driver);
            WebElement elementToClick = driver.findElement(By.xpath(elementLinkTextXpath));

            act.doubleClick(elementToClick).perform();
            System.out.println("Double-clicked element by Xpath : " + elementLinkTextXpath);
            Assert.assertTrue("Double-clicked element by Xpath : " + elementLinkTextXpath, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to double click element by link text  ---  " + elementLinkTextXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <doubleClickElement>
    public static void doubleClickElement(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            Actions act = new Actions(driver);
            WebElement elementToClick = driver.findElement(element);

            act.doubleClick(elementToClick).perform();
            System.out.println("Double-clicked element : " + element);
            Assert.assertTrue("Double-clicked element : " + element, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to double click element  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextByXpath>
    public static void enterTextByXpath(String elementXpath, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else if (textToEnter.equalsIgnoreCase("Clear")) {
                waitForElementByXpath(elementXpath, errorMessage);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.clear();
                Assert.assertTrue("Cleared text field", true);
            }
            else {
                waitForElementByXpath(elementXpath, errorMessage);
                Actions action = new Actions(driver);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.click();
                action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
                elementToTypeIn.sendKeys(textToEnter);

                System.out.println("Entered text : \"" + textToEnter + "\" to : " + elementXpath);
                Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + elementXpath, true);
            }
        }
        catch (Exception e) {
            System.out.println("[ERROR] Failed to enter text by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterText>
    public static void enterText(By element, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else if (textToEnter.equalsIgnoreCase("Clear")) {
                waitForElement(element, errorMessage);
                WebElement elementToTypeIn = driver.findElement(element);
                elementToTypeIn.clear();
                Assert.assertTrue("Cleared text field", true);
            }
            else {
                waitForElement(element, errorMessage);
                Actions action = new Actions(driver);
                WebElement elementToTypeIn = driver.findElement(element);
                elementToTypeIn.click();
                action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
                elementToTypeIn.clear();
                elementToTypeIn.click();
                elementToTypeIn.sendKeys(textToEnter);

                System.out.println("Entered text : \"" + textToEnter + "\" to : " + element);
                Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + element, true);
            }
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <extractTextByXpath>
    public static String extractTextByXpath(String elementXpath, String errorMessage) {

        String retrievedText = "";
        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToRead = driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();

            System.out.println("Text : " + retrievedText + " retrieved from element by Xpath - " + elementXpath);
            Assert.assertTrue("Text : " + retrievedText + " retrieved from element - " + elementXpath, true);

            return retrievedText;
        }
        catch (Exception e) {
            System.out.println("Failed to retrieve text from element Xpath - " + elementXpath + " - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to retrieve text from element - " + elementXpath + " - " + e.getMessage());

            return retrievedText;
        }
    }
    //endregion

    //region <extractText>
    public static String extractText(By element, String errorMessage) {

        String retrievedText = "";
        try {
            waitForElement(element, errorMessage);
            WebElement elementToRead = driver.findElement(element);
            retrievedText = elementToRead.getText();

            System.out.println("Text : " + retrievedText + " retrieved from element - " + element);
            Assert.assertTrue("Text : " + retrievedText + " retrieved from element - " + element, true);

            return retrievedText;
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to retrieve text from element - " + element + " - " + e.getMessage());

            return retrievedText;
        }
    }
    //endregion

    //region <hoverOverElement>
    public static void hoverOverElementAndClickSubElementByXpath(String elementXpath, String subElementToClickXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);

            Actions hoverTo = new Actions(driver);
            hoverTo.moveToElement(driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();
            pause(1000);
            hoverTo.moveToElement(driver.findElement(By.xpath(subElementToClickXpath)));
            hoverTo.click();
            hoverTo.perform();

            System.out.println("Hovered over element - " + elementXpath + " and clicked sub element " + subElementToClickXpath);
            Assert.assertTrue("Hovered over element - " + elementXpath + " and clicked sub element " + subElementToClickXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to hover over element and click sub element by Xpath  - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to hover over element Xpath : " + elementXpath + " and click sub element " + subElementToClickXpath + " ---  " + e.getMessage());
        }
    }

    public static void hoverOverElementAndClickSubElement(By element, By subElementToClick, String errorMessage) {

        try {
            waitForElement(element, errorMessage);

            Actions hoverTo = new Actions(driver);
            hoverTo.moveToElement(driver.findElement(element));
            hoverTo.perform();
            pause(1000);
            hoverTo.moveToElement(driver.findElement(subElementToClick));
            hoverTo.click();
            hoverTo.perform();

            System.out.println("Hovered over element - " + element + " and clicked sub element " + subElementToClick);
            Assert.assertTrue("Hovered over element - " + element + " and clicked sub element " + subElementToClick, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to hover over element : " + element + " and click sub element " + subElementToClick + " ---  " + e.getMessage());
        }
    }

    public static void hoverOverElement(By element, String errorMessage) {

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            WebElement ele = driver.findElement(element);
            Actions actions = new Actions(driver);
            actions.moveToElement(ele).build().perform();

            System.out.println("Hovered over element - " + element);
            Assert.assertTrue("Hovered over element - " + element, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("[ERROR] Failed to hover over element : " + element + " ---  " + e.getMessage());
        }
    }

    public static void hoverOverElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);

            Actions hoverTo = new Actions(driver);
            hoverTo.moveToElement(driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();

            System.out.println("Hovered over element - " + elementXpath);
            Assert.assertTrue("Hovered over element - " + elementXpath, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to hover over element Xpath : " + elementXpath + " ---  " + e.getMessage());
        }
    }
    //endregion

    //region <openUrlInNewTab>
    public static void openUrlInNewTab(String url, String errorMessage) {

        try {
            //Execute the JavaScript
            WebElement element = (WebElement)((JavascriptExecutor) driver).executeScript("window.open()");

            //Switch to the new tab
            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(1));

            //navigate to url
            driver.navigate().to(url);
            System.out.println("Opened new tab - " + driver.getTitle());
            Assert.assertTrue("Opened new tab - " + driver.getTitle(), true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to Open new tab ---  " + e.getMessage());
        }
    }
    //endregion

    //region <pasteCopiedTextByXpath>
    public static void pasteCopiedTextByXpath(String elementXpath, String value) {

        try {
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            Actions typeText = new Actions(driver);
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "a")); //Ctrl+a
            elementToTypeIn.sendKeys(Keys.DELETE); //Delete
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "v")); //Ctrl+v
            typeText.sendKeys(elementToTypeIn, value);

            System.out.println("Pasted copied text \"" + value + "\" by Xpath -- " + elementXpath);
            Assert.assertTrue("Pasted copied text \"" + value + "\" by Xpath -- " + elementXpath, true);
        }
        catch (Exception e) {
            System.out.println("Error pasting copied text \"" + value + "\"  - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to paste copied text by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <pasteCopiedText>
    public static void pasteCopiedText(By element, String value, String errorMessage) {

        try {
            WebElement elementToTypeIn = driver.findElement(element);
            Actions typeText = new Actions(driver);
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "a")); //Ctrl+a
            elementToTypeIn.sendKeys(Keys.DELETE); //Delete
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "v")); //Ctrl+v
            typeText.sendKeys(elementToTypeIn, value);

            System.out.println("Pasted copied text \"" + value + "\" -- " + element);
            Assert.assertTrue("Pasted copied text \"" + value + "\" -- " + element, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to paste copied text  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <pause>
    public static void pause(int millisecondsWait) {

        try {
            Thread.sleep(millisecondsWait);
        }
        catch (Exception e) {
        }
    }
    //endregion

    //region <refreshPage>
    public static void refreshPage() {
        try {
            driver.navigate().refresh();
            Assert.assertTrue("Refreshed page", true);
        }
        catch (Exception e) {
            System.out.println("Failed to refresh page - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to refresh page - " + e.getMessage());
        }
    }
    //endregion

    //region <rightClickOnElement>
    public static void rightClickOnElement(By element, String errorMessage) {
        try {
            waitForElement(element, errorMessage);
            Actions action = new Actions(driver);
            WebElement elementToRightClickOn = driver.findElement(element);
            action.contextClick(elementToRightClickOn).perform();
            Assert.assertTrue("Right-clicked", true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed Right-click - " + e.getMessage());
        }
    }
    //endregion

    //region <rightClickOnElementAndSelectOption>
    /**
     * This method can be use to right-click on an element and
     * select any option based on the option's name we explicitly specify
     */
    public static void rightClickOnElementAndSelectOption(By element, String elementToSelectXpath, String valueToSelect, String errorMessage) {

        String updatedXpath;
        try {
            waitForElement(element, errorMessage);
            Actions action = new Actions(driver);
            WebElement elementToRightClickOn = driver.findElement(element);
            action.contextClick(elementToRightClickOn).perform(); //right-click

            updatedXpath = updateXpathValueWithString(elementToSelectXpath, valueToSelect); //update default value saved in .properties
            waitForElementByXpath(updatedXpath, errorMessage);
            WebElement elementToClick = driver.findElement(By.xpath(updatedXpath));
            elementToClick.click();
            Assert.assertTrue("Right-clicked and selected \"" + valueToSelect + "\"", true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed Right-click and select \"" + valueToSelect + "\" - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDown>
    public static void scrollDown() {

        try {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("window.scrollBy(0,250)", "");
            System.out.println("Scrolled Down");
            Assert.assertTrue("Scrolled Down", true);
        }
        catch (Exception e) {
            System.out.println("Failed to scroll Down - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to scroll Down - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollUp>
    public static void scrollUp() {

        try {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("window.scrollBy(0,-250)", "");
            System.out.println("Scrolled Up");
            Assert.assertTrue("Scrolled Up", true);
        }
        catch (Exception e) {
            System.out.println("Failed to scroll Up - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to scroll Up - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElementByXpath>
    public static void scrollToElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement element = driver.findElement(By.xpath(elementXpath));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            Thread.sleep(500);

            System.out.println("Scrolled To Element - " + elementXpath);
            Assert.assertTrue("Scrolled To Element - " + elementXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to scroll To Element --- " + elementXpath + " - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to scroll To Element - " + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElement>
    public static void scrollToElement(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elmt = driver.findElement(element);
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elmt);
            Thread.sleep(500);

            System.out.println("Scrolled To Element - " + element);
            Assert.assertTrue("Scrolled To Element - " + element, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to scroll To Element - "  + element + " - " + e.getMessage());
        }
    }
    //endregion

    //region <selectFromDropDownListByXpath>
    public static void selectFromDropDownListByXpath(String dropdownlistXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(dropdownlistXpath, valueToSelect); //update default value saved in .properties
            waitForElementByXpath(updatedXpath, errorMessage);
            Select dropDownList = new Select(driver.findElement(By.xpath(updatedXpath)));
            WebElement formxpath = driver.findElement(By.xpath(updatedXpath));
            formxpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + updatedXpath);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + updatedXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to select from dropdown list by text using Xpath - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }

    public static void selectFromDropDownList(By dropdownList, String valueToSelect, String errorMessage) {

        try {
            waitForElement(dropdownList, errorMessage);
            Select dropDownList = new Select(driver.findElement(dropdownList));
            WebElement formxpath = driver.findElement(dropdownList);
            formxpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + dropdownList);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + dropdownList, true);
        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }

    //Dynamic method to select value from dropdown list
    public static void selectFromDropDownList_MrDExpressByXpath(String dropdownListXpath, String valueToSelect, String errorMessage) {

        try {
            String xp = updateXpathValueWithString(dropdownListXpath, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(xp, errorMessage);
            Select dropDownList = new Select(driver.findElement(By.xpath(xp)));
            WebElement formxpath = driver.findElement(By.xpath(xp));
            formxpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + xp);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + xp, true);
        }
        catch (Exception e) {
            System.out.println("Failed to select from dropdown list by text using Xpath - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <selectFromDropdownListViaDivUsingXpath>
    public static void selectFromDropdownListViaDivUsingXpath(String dropdownlistXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(dropdownlistXpath, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(updatedXpath, errorMessage);
            WebElement dropDownList = driver.findElement(By.xpath(dropdownlistXpath));
            dropDownList.click();
            dropDownList = driver.findElement(By.xpath(valueToSelect));
            Actions select = new Actions(driver);
            select.moveToElement(dropDownList);
            select.click(dropDownList);
            select.perform();

            System.out.println("Selected Text : " + valueToSelect + " from : " + updatedXpath);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + updatedXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to select from dropdown list by text using Xpath - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <sendKeys>
    public static void sendKeys(String choice) {

        try {
            Actions action = new Actions(driver);

            switch (choice.toUpperCase()) {
                case "ARROW DOWN": {
                    action.sendKeys(Keys.ARROW_DOWN);
                    action.perform();
                    System.out.println("Pressed : " + choice);
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "ENTER": {
                    action.sendKeys(Keys.ENTER);
                    action.perform();
                    System.out.println("Pressed : " + choice);
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "TAB": {
                    action.sendKeys(Keys.TAB);
                    action.perform();
                    System.out.println("Pressed : " + choice);
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "COPY ALL": {
                    action.sendKeys(Keys.CONTROL, "a"); //Ctrl+a
                    action.sendKeys(Keys.CONTROL, "c"); //Ctrl+c
                    action.perform();
                    System.out.println("Pressed : " + choice);
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "REFRESH": {
                    action.keyDown(Keys.CONTROL);
                    action.sendKeys(Keys.F5);
                    action.keyUp(Keys.CONTROL);
                    action.perform();
                    System.out.println("Pressed : " + choice);
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
            }
        }
        catch (Exception e) {
            System.out.println("Failed to send keypress : \"" + choice + "\"");
            Assert.fail("\n[ERROR] Failed to send keypress : \"" + choice + "\" --- " + e.getMessage());
        }
    }
    //endregion

    //region <switchToDefaultContent>
    public static void switchToDefaultContent() {

        try {
            driver.switchTo().defaultContent();
            System.out.println("Switched to default content");
            Assert.assertTrue("Switched to default content", true);
        }
        catch (Exception e) {
            System.out.println("Failed to switch to default content - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to switch to default content - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToTabOrWindow>
    public static void switchToTabOrWindow() {

        try {
            String winHandleBefore = driver.getWindowHandle();

            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
                System.out.println("Switched to window : " + driver.getTitle());
                Assert.assertTrue("Switched to window : " + driver.getTitle(), true);
            }
        }
        catch (Exception e) {
            System.out.println("Could not switch to new tab" + e.getMessage());
            Assert.fail("\n[ERROR] Failed to switch to new tab - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToPreviousTab>
    public static void switchToPreviousTab() {

        try {
            //Switch back to first tab
            driver.switchTo().window(driver.getWindowHandle());

            System.out.println("Switched To Previous Tab - " + driver.getTitle());
            Assert.assertTrue("Switched To Previous Tab - " + driver.getTitle(), true);
        }
        catch (Exception e) {
            System.out.println("Failed to Switch To Previous Tab - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to Switch To Previous Tab - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToFrameByXpath>
    public static void switchToFrameByXpath(String frameXpath) {

        boolean switchSuccessful;
        try {
            WebDriver frame = new WebDriverWait(driver, WaitTimeout).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameXpath));
            frame.switchTo().activeElement();
            switchSuccessful = true;
            System.out.println("Switched to frame - " + frameXpath);
            Assert.assertTrue("Switched to frame - " + frameXpath, true);
        }
        catch (Exception e) {
            switchSuccessful = false;
            System.out.println("Failed to switch to frame by Xpath - " + frameXpath + " - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to switch to frame by Xpath - " + frameXpath + " - " + e.getMessage());
        }

        switchToElement(switchSuccessful);
    }

    private static boolean switchToElement(boolean switchSuccessful) {
        return switchSuccessful;
    }
    //endregion

    //region <switchToFrame>
    public static void switchToFrame(By frame_, String errorMessage) {

        boolean switchSuccessful;
        try {
            WebDriver frame = new WebDriverWait(driver, WaitTimeout).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame_));
            frame.switchTo().activeElement();
            switchSuccessful = true;
            System.out.println("Switched to frame - " + frame_);
            Assert.assertTrue("Switched to frame - " + frame_, true);
        }
        catch (Exception e) {
            switchSuccessful = false;
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to switch to frame - " + frame_ + " - " + e.getMessage());
        }

        switchToElement_(switchSuccessful);
    }

    private static boolean switchToElement_(boolean switchSuccessful) {
        return switchSuccessful;
    }
    //endregion

    //region <To update an Xpath value from the .properties file>
    public static String updateXpathValueWithString(String xp, String value) {
        String xpath = xp.replace("value", value);
        return xpath;
    }
    //endregion

    //region <validateSubstringIsPresent>
    public static void validateSubstringIsPresent(String string, String substring, String errorMessage) {

        boolean isFound = string.contains(substring);
        if (isFound) {
            System.out.println("Substring: \"" + substring + "\" is present");
            Assert.assertTrue("Substring: \"" + substring + "\" is present", true);
        }
        else {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Substring: \"" + substring + "\" is not present");
        }
    }
    //endregion

    //region <waitForElementByXpath>
    public static void waitForElementByXpath(String elementXpath, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        System.out.println("Found element by Xpath : " + elementXpath);
                        Assert.assertTrue("Found element by Xpath : " + elementXpath, true);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    System.out.println(errorMessage);
                    Assert.fail("\n[ERROR] Did Not Find element by Xpath : " + elementXpath + "' - " + e.getMessage());
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                System.out.println("Reached TimeOut whilst waiting for element by Xpath : '" + elementXpath + "'");
            }

        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }

    private static boolean GetElementFound(boolean elementFound) {
        return elementFound;
    }

    private static boolean GetElementFound1(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElement>
    public static void waitForElement(By element, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(element));
                    wait.until(ExpectedConditions.elementToBeClickable(element));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(element)) != null) {
                        elementFound = true;
                        System.out.println("Found element : " + element);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    System.out.println(errorMessage);
                    Assert.fail("\n[ERROR] Did Not Find element : " + element + "' - " + e.getMessage());
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1_(elementFound);
                System.out.println("Reached TimeOut whilst waiting for element : '" + element + "'");
            }

        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to wait for element --- " + element);
        }

        GetElementFound_(elementFound);
    }

    private static boolean GetElementFound_(boolean elementFound) {
        return elementFound;
    }

    private static boolean GetElementFound1_(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForOptionByXpath>
    /*Dynamic method to wait for an element Xpath. Using a dynamic Xpath of the element, we just specify the value to select*/
    public static void waitForOptionByXpath(String elementXpath, String valueToSelect, String errorMessage) {

        boolean elementFound = false;
        String updatedXpath = "";
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);
                    updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(updatedXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(updatedXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(updatedXpath))) != null) {
                        elementFound = true;
                        Assert.assertTrue("Found element by Xpath : " + updatedXpath, true);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    System.out.println(errorMessage);
                    Assert.fail("\n[ERROR] Did Not Find element by Xpath : " + updatedXpath + "' - " + e.getMessage());
                }
                //Thread.sleep(500);
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                System.out.println("Reached TimeOut whilst waiting for element by Xpath : '" + updatedXpath + "'");
            }

        }
        catch (Exception e) {
            System.out.println(errorMessage);
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + updatedXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }
    //endregion
}
